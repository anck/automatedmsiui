'''
Created on Feb 6 2016

@author: Anchitk
'''

from citrixframeworkclasses import ICAWSMSIWindowsHandler
from configuration import ICAWSMSIConfig_86
from installerspecificframeworkclasses import DummyMSIDialogueBox
from testframeworkclasses import ExecutePowershellCommand
from testframeworkclasses import LaunchMSI
from testframeworkclasses import MsiInstaller, MSIFrameworkLogging

MSIFrameworkLogging.MSIFrameworkLogging.Initialize()
MsiInstaller.MsiInstaller.Intialize()
commandToMountSharedFolder = 'net use ' + ICAWSMSIConfig_86.ICAWSDriveToMountPath + ' "' + ICAWSMSIConfig_86.ICAWSPath + '" /user:' + ICAWSMSIConfig_86.ICAWSuser + " " + ICAWSMSIConfig_86.ICAWSpwd
ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(commandToMountSharedFolder)
LaunchMSI.LaunchMSI.withName(ICAWSMSIConfig_86.ICAWSName).atLocation(ICAWSMSIConfig_86.ICAWSDriveToMountPath).launch()
ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.initializeMSIWindowHandler()

def testStatic2LicenseAgremmentHeader():
    print("LicenseAgreementHeader: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.getLicenseAgreementHeader())
    print("LicenseAgreement: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.getLicenseAgreement())
    print("InstallButtonEnabled: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isInstallButtonEnabled())
    print("PrintButtonEnabled: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isPrintButtonEnabled())
    print("BackButtonEnabled: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isBackButtonEnabled())
    print("CancelButtonEnabled: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isCancelButtonEnabled())
    print("LicenseAgreementAcceptCheckboxText: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.getLicenseAgreementAcceptCheckboxText())
    print("ClickCheckboxToAcceptLicenseAgreement: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.clickCheckboxToAcceptLicenseAgreement())
    print("InstallButtonEnabled: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isInstallButtonEnabled())
    print("clickInstallButton: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.clickInstallButton())
    print("clickCancelButton: %s" % ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.clickCancelButton())
    print("MSIDialogueBox: %s" % DummyMSIDialogueBox.MSIDialogueBox.msiDialogueBoxWindowTitle())
    print("MSIDialogueBox Message:  %s" % DummyMSIDialogueBox.MSIDialogueBox.msiDialogueBoxMessage())
    print("MSIDialogueBox click yes:  %s" % DummyMSIDialogueBox.MSIDialogueBox.clickYesButton())
    #print("MSIDialogueBox click yes:  %s" % MSIDialogueBox.MSIDialogueBox.msiDialogueBoxClickNo())
    print("MsiInstaller Close:  %s" % MsiInstaller.MsiInstaller.close())
    print("MsiInstaller Kill Msiexec:  %s" % MsiInstaller.MsiInstaller.killInstaller())


testStatic2LicenseAgremmentHeader()
