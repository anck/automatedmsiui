'''
Created on Jan 25, 2016

@author: Anchitk
'''
import sys, os
import pywinauto
from .MsiInstaller import MsiInstaller


class LaunchCommand(object):
    '''
    classdocs
    '''
    fileName =""
    localDirectory =""


    def __init__(self, _fileName):
        '''
        Constructor
        '''
        self.fileName = _fileName
    
    def atLocation(self, _localDirectroy):
        self.localDirectory = _localDirectroy
        return self
        
    def launch(self):
        #MsiInstaller.Instance() = pywinauto.application.Application()
        comp_path = self.localDirectory + "\\" + self.fileName
        MsiInstaller.Instance().start("msiexec /i" + comp_path)
        MsiInstaller.WaitTillEnabledAndReady()
