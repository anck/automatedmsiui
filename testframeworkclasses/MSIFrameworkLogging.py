__author__ = 'Anchitk'
import logging



class MSIFrameworkLogging(object):

    logisset = None

    @staticmethod
    def Initialize():
        if MSIFrameworkLogging.logisset == None:
            #MSIFrameworkLogging.logisset = logging.basicConfig(filename="MSIInstallFrameworkLogging.log", level=logging.DEBUG)
            MSIFrameworkLogging.logisset = logging.basicConfig(level=logging.DEBUG)

    """ TODO: improve upon this.
    def logIt(level, logMessage)
        return
        {
            'DEBUG': logging.debug(),
            'CRITICAL': logging.critical(),
            'ERROR': logging.error(),
            }.get("DEBUG", logging.debug())
    """
    @staticmethod
    def logIt(level, className, logMessage):
        logging.debug(level + ":" + className + ":" + logMessage)