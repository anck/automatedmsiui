__class__ = "MsiInstaller"
'''
Created on Jan 25, 2016

@author: Anchitk
'''
from builtins import staticmethod
import pywinauto
from testframeworkclasses import MSIFrameworkLogging
from pywinauto.timings import Timings
from testframeworkclasses import ExecutePowershellCommand
import time


'''
Singelaton class. Wrapper for pywinauto Application
'''
class MsiInstaller(object):
    """
    classdocs
    """
    _pwa_msiInstaller = None


    """
    As this a Singelaton class, initializer should not be called.
    """
    def __init__(self, params):
        '''
        Constructor
        '''
        print("please use the Intialize method")

    """
    Initializes the PWA application or if already set it returns the Instance of the PWA application
    """
    @staticmethod
    def Intialize():
        if MsiInstaller._pwa_msiInstaller == None :
            MsiInstaller._pwa_msiInstaller = pywinauto.application.Application()
            MSIFrameworkLogging.MSIFrameworkLogging.logIt("debug" , "MsiInstaller.Initialize" , " _pwa_msiInstaller new value initialized" )
            Timings.Slow()

        MSIFrameworkLogging.MSIFrameworkLogging.logIt("debug" , "MsiInstaller.Initialize" , " _pwa_msiInstaller already initialized. Call Instance to get the value " )

    """
    Returns the Instance of the Application
    """
    @staticmethod
    def Instance():
        MSIFrameworkLogging.MSIFrameworkLogging.logIt("debug" , "MsiInstaller.Instance" , " returning class variable" )
        return MsiInstaller._pwa_msiInstaller
    """
    Function returns the MainMSI window handle.
    TODO: remove the Hardcoded value MSIDialogueCloseClass
    """
    @staticmethod
    def GetMsiWindowHandle():
        #TODO:remove the HardCoded Value
        return MsiInstaller.Instance().MsiDialogCloseClass

    """
    Function waits for MainMSI window to get enabled.
    """
    @staticmethod
    def WaitTillEnabledAndReady():
        time.sleep(2)
        MsiInstaller.GetMsiWindowHandle().Wait('enabled visible ready')

    """
    Function waits for MsiDialogCloseClass in increments of 0.5 seconds.
    """
    @staticmethod
    def waitInIntervals():
        time.sleep(1)
        pywinauto.timings.WaitUntilPasses(20, 0.5, lambda: MsiInstaller.Instance().window_(Class = "MsiDialogCloseClass"))

    """
    Waits for the given interval
    """
    @staticmethod
    def waitForSecond(interval):
        time.sleep(float(interval))

    """
    return the window title
    """
    @staticmethod
    def getWindowTitle():
        return MsiInstaller.GetMsiWindowHandle().WindowText()

    """
    Function calls the PWA Close window on the Window Handle
    """
    @staticmethod
    def close():
        return MsiInstaller.GetMsiWindowHandle().Close()

    """
    Function calls 'taskkill /im msiexec.exe /F' on a WindowsOS CMD.
    To be used for clean up after all tests have completed to close and lingering on msiexec processes.
    """
    @staticmethod
    def killInstaller():
        return ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess("taskkill /im msiexec.exe /F")
