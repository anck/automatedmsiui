"""
__author__ = 'Anchitk'
"""

from testframeworkclasses import MsiInstaller
from testframeworkclasses import MSIFrameworkLogging

"""
TODO: convert this into an interface\\abstract class
Class provides access to the MSI window for ICA WS MSI
"""
class ICAWSMSIWindowsHandler(object):

    local_pwa_installer_WindowHandle = None

    """
    Initializes the local variable with the windows handle.
    """
    @staticmethod
    def initializeMSIWindowHandler():
        ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle = MsiInstaller.MsiInstaller.GetMsiWindowHandle()
        MsiInstaller.MsiInstaller.waitInIntervals()
        MsiInstaller.MsiInstaller.WaitTillEnabledAndReady()

    """
    """
    @staticmethod
    def getLocalPwaInstallerWindowHandler():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle;


    """
    """
    @staticmethod
    def getLicenseAgreementHeader():
        "Wait time to start testing Installer Window."
        '''TODO: Change wait to be more dynamic'''
        #MsiInstaller.MsiInstaller.waitForSecond(5)
        #MsiInstaller.MsiInstaller.waitInIntervals()
        #MsiInstaller.MsiInstaller.WaitTillEnabledAndReady()
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Static2.WindowText()

    @staticmethod
    def getLicenseAgreement():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.RichEdit20W.WindowText()
    @staticmethod
    def getLicenseAgreementAcceptCheckboxText():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.ChcekBox.WindowText()
    @staticmethod
    def clickCheckboxToAcceptLicenseAgreement():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.ChcekBox.Click()
    @staticmethod
    def isInstallButtonEnabled():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Install.IsEnabled()
    @staticmethod
    def clickInstallButton():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Install.Click()
    @staticmethod
    def isBackButtonEnabled():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Back.IsEnabled()
    @staticmethod
    def clickBackButton():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Back.Click()
    @staticmethod
    def isPrintButtonEnabled():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Print.IsEnabled()
    @staticmethod
    def clickPrintButton():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Print.Click()
    @staticmethod
    def isCancelButtonEnabled():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Cancel.IsEnabled()
    @staticmethod
    def clickCancelButton():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Cancel.Click()
    @staticmethod
    def isFinishButtonEnabled():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Finish.IsEnabled()
    @staticmethod
    def clickFinishButton():
        return ICAWSMSIWindowsHandler.local_pwa_installer_WindowHandle.Finish.Click()