import os.path

MSIName="Detection.msi"
MSIDriveName = "D:\\"
MSIDriveType = "local"
MSIPath= os.path.join( MSIDriveName, "projects" , "automatedmsiui" , "DummyInstaller" )


MSIWindowTitle = "System Requirements Lab Detection Setup"
MSIDescriptionTitle = "Welcome to the System Requirements Lab Detection Setup Wizard"
'''
MSIEULAHeader = "Please read the Desktop OS VDA Core Services License Agreement"
MSISEulaMessage = 'CITRIX LICENSE AGREEMENT\r\n\r\nUse of this component is subject to the Citrix license or terms of service covering the Citrix product(s) or service(s) with which you will be using this component. This component is licensed for use only with such Citrix product(s) or service(s).\r\n\r\nCTX_code EP_R_A124358'
MSILicenseAgreementAcceptCheckboxText = "I &accept the terms in the License Agreement"
'''
MSIDialogueBoxMessage = "Are you sure you want to cancel?"

MSISuser = "eng\\svc_automation"
MSIpwd = "autom@t3"
'''
MSISuser = "eng\\svc_automation"
MSIpwd = "autom@t3"
'''