"""
__author__ = 'Anchitk'
"""

from configuration import DetectionConfig_local
from testframeworkclasses import LaunchMSI, ExecutePowershellCommand, MsiInstaller


class DummyMSIInstaller(object):

    @staticmethod
    def initializeAndLaunch():
        MsiInstaller.MsiInstaller.Intialize()
        if DetectionConfig_local.MSIDriveType != "local":
            commandToMountSharedFolder = 'net use ' + DetectionConfig_local.MSIPath
            ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(commandToMountSharedFolder)
        LaunchMSI.LaunchMSI.withName(DetectionConfig_local.MSIName).atLocation(DetectionConfig_local.MSIPath).launch()


    @staticmethod
    def deinitializeAndClose():
        MsiInstaller.MsiInstaller.close()
        MsiInstaller.MsiInstaller.killInstaller()
        if(DetectionConfig_local.MSIDriveType != "local"):
            command = 'net use '+ DetectionConfig_local.MSIDriveName +' /del /yes'
            ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(command)
        else:
            print ("Drive is local can not unmount")