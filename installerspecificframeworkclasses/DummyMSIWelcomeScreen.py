"""
__author__ = 'Anchitk'
"""

from testframeworkclasses import MsiInstaller
from testframeworkclasses import MSIFrameworkLogging

"""
TODO: convert this into an interface\\abstract class
Class provides access to the MSI window for ICA WS MSI
"""
class DummyMSIWelcomeScreen(object):

    local_pwa_installer_WindowHandle = None

    """
    Initializes the local variable with the windows handle.
    """
    @staticmethod
    def connectWelcomeScreenToHandle():
        DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle = MsiInstaller.MsiInstaller.GetMsiWindowHandle()
        MsiInstaller.MsiInstaller.waitInIntervals()
        MsiInstaller.MsiInstaller.WaitTillEnabledAndReady()

    """
    """
    @staticmethod
    def isAt():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle != None


    """
    """
    @staticmethod
    def getHeaderText():
        "Wait time to start testing Installer Window."
        '''TODO: Change wait to be more dynamic'''
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Static3.WindowText()

    @staticmethod
    def getDescriptionText():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Static2.WindowText()
    @staticmethod
    def isNextButtonEnabled():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Next.IsEnabled()
    @staticmethod
    def clickNextButton():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Next.Click()
    @staticmethod
    def isBackButtonEnabled():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Back.IsEnabled()
    @staticmethod
    def clickBackButton():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Back.Click()
    @staticmethod
    def isCancelButtonEnabled():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Cancel.IsEnabled()
    @staticmethod
    def clickCancelButton():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Cancel.Click()

    @staticmethod
    def clickCancelButton():
        return DummyMSIWelcomeScreen.local_pwa_installer_WindowHandle.Cancel.Click()

    @staticmethod
    def getWindowTitle():
        return MsiInstaller.MsiInstaller.getWindowTitle();