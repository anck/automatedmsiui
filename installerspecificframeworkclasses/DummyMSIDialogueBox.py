"""
This module acts as a wrapper for any dialogue  boxes which open up.
"""
__class__ = "MSIDialogueBox"

from testframeworkclasses import MsiInstaller
from testframeworkclasses import MSIFrameworkLogging

"""
Class is a wrapper fpr dialogue boxes opened by an MSI installer
"""
class MSIDialogueBox(object):

    _dialogueBoxWrapper = None

    """
    Returns the Dialogue Box Instance.
    """
    @staticmethod
    def msiDialogueBoxInstance():
        MSIFrameworkLogging.MSIFrameworkLogging.logIt("DEBUG", str(__class__) ,"msiDialogueBoxInstance: returning top window")
        return MsiInstaller.MsiInstaller.Instance().top_window_()


    """
    Returns the Window Title.
    """
    @staticmethod
    def msiDialogueBoxWindowTitle():
        MSIFrameworkLogging.MSIFrameworkLogging.logIt("DEBUG", str(__class__) , "msiDialogueBoxWindowTitle: Window Text")
        return MSIDialogueBox.msiDialogueBoxInstance().WindowText()

    """
    Clicks the Yes Button on the dialogue box.
    """
    @staticmethod
    def clickYesButton():
        MSIFrameworkLogging.MSIFrameworkLogging.logIt("DEBUG", str(__class__) ,"msiDialogueBoxClickYes")
        return MSIDialogueBox.msiDialogueBoxInstance().Yes.Click()

    """
    Clicks the No Button on the dialogue box.
    """
    @staticmethod
    def clickNoButton():
        MSIFrameworkLogging.MSIFrameworkLogging.logIt("DEBUG", str(__class__) ,"msiDialogueBoxClickNo")
        return MSIDialogueBox.msiDialogueBoxInstance().No.Click()

    """
    Returns Message presented on the Dialogue box.
    """
    @staticmethod
    def msiDialogueBoxMessage():
        MSIFrameworkLogging.MSIFrameworkLogging.logIt("DEBUG", str(__class__) ,"msiDialogueBoxMessage")
        return MSIDialogueBox.msiDialogueBoxInstance().Static1.WindowText()