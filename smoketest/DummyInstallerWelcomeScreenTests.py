'''
Created on Jan 25, 2016

@author: Anchitk
'''
import unittest

from installerspecificframeworkclasses import DummyMSIInstaller, DummyMSIDialogueBox
#from testframeworkclasses import LaunchMSI, ExecutePowershellCommand, MsiInstaller, MSIDialogueBox, MSIFrameworkLogging
from installerspecificframeworkclasses import DummyMSIWelcomeScreen
from configuration import DetectionConfig_local


class DummyInstallerWelcomeScreenTests(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        DummyMSIInstaller.DummyMSIInstaller.initializeAndLaunch()
        DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.connectWelcomeScreenToHandle()


    def test_01IsTitleOfDescriptionExpected(self):
        self.assertEqual(DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.getHeaderText(), str(DetectionConfig_local.MSIDescriptionTitle))
        print("completed test_01IsTextHeaderExpected")

    def test_02IsDescriptionExpected(self):
         self.assertEqual("this is not expected!", DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.getDescriptionText())
         print("completed test_02IsDescriptionExpected")

    def test_03IsNextButtonEnabled(self):
        self.assertTrue(DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.isNextButtonEnabled())
        print("completed test_03IsNextButtonEnabled")

    def test_05IsBackButtonEnabled(self):
         self.assertEquals(DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.isBackButtonEnabled(), False)
         print("completed test_05IsBackButtonEnabled")

    def test_06IsCancelButtonEnabled(self):
        self.assertEquals(DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.isCancelButtonEnabled(), True)
        print("completed test_06IsCancelButtonEnabled")

    def test_11ClickCancelButtonBeforeInstallationStarted(self):
        DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.clickCancelButton()
        print("completed test_11ClickCancelButtonBeforeInstallationStarted")

    def test_12CheckMSIDialogueBoxPromptHeader(self):
        self.assertEquals(str(DetectionConfig_local.MSIDialogueBoxMessage), DummyMSIDialogueBox.MSIDialogueBox.msiDialogueBoxWindowTitle() )
        print("completed test_12CheckMSIDialogueBoxPromptHeader");

    def test_14ClickMSIDialogueBoxYesToCancelInstallation(self):
        print("debug")
        DummyMSIDialogueBox.MSIDialogueBox.clickYesButton()
        print("completed test_14ClickMSIDialogueBoxYesToCancelInstallation")

    @classmethod
    def tearDownClass(cls):
        DummyMSIInstaller.DummyMSIInstaller.deinitializeAndClose()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
