'''
Created on Jan 25, 2016

@author: Anchitk
'''
import unittest

from citrixframeworkclasses import ICAWSMSIWindowsHandler
from configuration import ICAWSMSIConfig_86
from installerspecificframeworkclasses import DummyMSIDialogueBox
from testframeworkclasses import LaunchMSI, ExecutePowershellCommand, MsiInstaller


class ICAWSWelcomeScreenTests(unittest.TestCase):

    filePath = "" #"\\\\eng.citrite.net\\ftl\\CitrixRTM\\CDS\\Avengers-VDA-7.6.FP3\\build_7020\\Image-Full\\x64\\Virtual Desktop Components\\WS"


    @classmethod
    def setUpClass(cls):
        MsiInstaller.MsiInstaller.Intialize()
        commandToMountSharedFolder = 'net use ' + ICAWSMSIConfig_86.ICAWSDriveToMountPath + ' "' + ICAWSMSIConfig_86.ICAWSPath + '" /user:' + ICAWSMSIConfig_86.ICAWSuser + " " + ICAWSMSIConfig_86.ICAWSpwd
        #TODO: comment this line if you don't want to mount a share.
        ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(commandToMountSharedFolder)
        LaunchMSI.LaunchMSI.withName(ICAWSMSIConfig_86.ICAWSName).atLocation(ICAWSMSIConfig_86.ICAWSDriveToMountPath).launch()
        ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.initializeMSIWindowHandler()


    def test_01LicenseAgremmentHeader(self):
        self.assertEqual(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.getLicenseAgreementHeader(), str(ICAWSMSIConfig_86.ICAWSEULAHeader))
        print ("completed test_01LicenseAgremmentHeader")

    def test_02LicenseAgremment(self):
         self.assertEqual(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.getLicenseAgreement(), str(ICAWSMSIConfig_86.ICAWSEulaMessage))
         print("completed test_02LicenseAgremment")

    def test_03InstallButtonEnabled(self):
        self.assertEquals(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isInstallButtonEnabled(), False)
        print("completed test_03InstallButtonEnabled")

    def test_04PrintButtonEnabled(self):
         self.assertEquals(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isPrintButtonEnabled(), True)
         print("completed test_04PrintButtonEnabled")

    def test_05BackButtonEnabled(self):
         self.assertEquals(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isBackButtonEnabled(), False)
         print("completed test_05BackButtonEnabled")

    def test_06CancelButtonEnabled(self):
        self.assertEquals(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isCancelButtonEnabled(), True)
        print("completed test_06CancelButtonEnabled")

    def test_07LicenseAgreementAcceptCheckboxText(self):
        self.assertEquals(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.getLicenseAgreementAcceptCheckboxText(),  str(ICAWSMSIConfig_86.LicenseAgreementAcceptCheckboxText))
        print("completed test_07LicenseAgreementAcceptCheckboxText")

    def test_08ClickCheckboxToAcceptLicenseAgreement(self):
        ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.clickCheckboxToAcceptLicenseAgreement()
        print("completed test_08ClickCheckboxToAcceptLicenseAgreement")

    def test_09InstallButtonEnabledAfterAcceptingLicenseAgreement(self):
        self.assertEquals(ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.isInstallButtonEnabled(), True)
        print("completed test_09InstallButtonEnabledAfterAcceptingLicenseAgreement")

    def test_10ClickInstallButtonAfterAcceptingLicenseAgreement(self):
        ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.clickInstallButton()
        print("completed test_10ClickInstallButtonAfterAcceptingLicenseAgreement")

    def test_11ClickCancelButtonAfterInstallationStarted(self):
         ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.clickCancelButton()
         print("completed test_11ClickCancelButtonAfterInstallationStarted")

    def test_12CheckMSIDialogueBoxPromptHeaderToAcceptCancelInstallation(self):
        self.assertEquals(DummyMSIDialogueBox.MSIDialogueBox.msiDialogueBoxWindowTitle(), str(ICAWSMSIConfig_86.ICAWSDialogueBoxHeader))
        print("completed test_12CheckMSIDialogueBoxPromptHeaderToAcceptCancelInstallation")

    def test_13CheckMSIDialogueBoxPromptMessageToAcceptCancelInstallation(self):
        self.assertEquals(DummyMSIDialogueBox.MSIDialogueBox.msiDialogueBoxMessage(), str(ICAWSMSIConfig_86.ICAWSDialogueBoxMessage))
        print("completed test_13CheckMSIDialogueBoxPromptMessageToAcceptCancelInstallation")

    def test_14ClickMSIDialogueBoxYesToCancelInstallation(self):
        DummyMSIDialogueBox.MSIDialogueBox.clickYesButton()
        print("completed test_14ClickMSIDialogueBoxYesToCancelInstallation")

    @classmethod
    def tearDownClass(cls):
        MsiInstaller.MsiInstaller.close()
        MsiInstaller.MsiInstaller.killInstaller()
        command = 'net use '+ ICAWSMSIConfig_86.ICAWSDriveToMountPath +' /del /yes'
        ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(command)
        print("completed teardown for ICAWSWelcomeScreenTests")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
