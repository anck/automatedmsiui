'''
Created on Jan 25, 2016

@author: Anchitk
'''
#sys.path.append("W:\python_work\\automatedmsiui\smoketest")
import unittest
from citrixframeworkclasses import ICAWSMSIWindowsHandler
from testframeworkclasses import LaunchMSI, ExecutePowershellCommand
from testframeworkclasses import MsiInstaller
from configuration import ICAWSMSIConfig_86


class LaunchMSITests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        MsiInstaller.MsiInstaller.Intialize()
        commandToMountSharedFolder = 'net use ' + ICAWSMSIConfig_86.ICAWSDriveToMountPath + ' "' + ICAWSMSIConfig_86.ICAWSPath + '" /user:' + ICAWSMSIConfig_86.ICAWSuser + " " + ICAWSMSIConfig_86.ICAWSpwd
        ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(commandToMountSharedFolder)

    def testLaunchMSITest(self):
        LaunchMSI.LaunchMSI.withName(ICAWSMSIConfig_86.ICAWSName).atLocation(ICAWSMSIConfig_86.ICAWSDriveToMountPath).launch()
        ICAWSMSIWindowsHandler.ICAWSMSIWindowsHandler.initializeMSIWindowHandler()

    def testMSIWindowName(self):
        MsiInstaller.MsiInstaller.WaitTillEnabledAndReady()
        self.assertEqual(MsiInstaller.MsiInstaller.getWindowTitle(), ICAWSMSIConfig_86.ICAWSWindowTitle, "The title of the MSI window is incorrect")

    @classmethod
    def tearDownClass(cls):
        MsiInstaller.MsiInstaller.close()
        MsiInstaller.MsiInstaller.killInstaller()
        command = 'net use '+ ICAWSMSIConfig_86.ICAWSDriveToMountPath +' /del /yes'
        ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(command)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

