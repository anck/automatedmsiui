'''
Created on Jan 25, 2016

@author: Anchitk
'''
#sys.path.append("W:\python_work\\automatedmsiui\smoketest")
import unittest

from configuration import DetectionConfig_local
from installerspecificframeworkclasses import DummyMSIWelcomeScreen
from testframeworkclasses import LaunchMSI, ExecutePowershellCommand
from testframeworkclasses import MsiInstaller


class LaunchMSITests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        MsiInstaller.MsiInstaller.Intialize()
        print(DetectionConfig_local.MSIPath)
        if DetectionConfig_local.MSIDriveType != "local":
            commandToMountSharedFolder = 'net use ' + DetectionConfig_local.MSIPath
            ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(commandToMountSharedFolder)

    def test_01LaunchMSITest(self):
        LaunchMSI.LaunchMSI.withName(DetectionConfig_local.MSIName).atLocation(DetectionConfig_local.MSIPath).launch()
        DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.connectWelcomeScreenToHandle()
        self.assertTrue(DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.isAt(), "MSI was not launched! Test failed")


    def test_02MSIWindowTitle(self):
        MsiInstaller.MsiInstaller.WaitTillEnabledAndReady()
        self.assertEqual(DummyMSIWelcomeScreen.DummyMSIWelcomeScreen.getWindowTitle(), DetectionConfig_local.MSIWindowTitle, "The title of the MSI window is incorrect")

    @classmethod
    def tearDownClass(cls):
        MsiInstaller.MsiInstaller.close()
        MsiInstaller.MsiInstaller.killInstaller()
        if DetectionConfig_local.MSIDriveType != "local":
            command = 'net use '+ DetectionConfig_local.MSIDriveName +' /del /yes'
            ExecutePowershellCommand.ExecutePowershellCommand.callSubProcess(command)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

